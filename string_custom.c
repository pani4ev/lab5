#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int concat(char** str1, char** str2, char** str_c)
{
    *str_c = malloc(sizeof(*str1) + sizeof(*str2));
    strcat(*str_c, *str1);
    strcat(*str_c, *str2);
    return 0;
}

int split(char** strs, char* str, int n, int max_len)
{
    if(strlen(str) <= n)
        return 1;
    else {
        strs[0] = malloc(max_len*sizeof(char));
        strs[1] = malloc(max_len*sizeof(char));
        strncpy(strs[0], str, (size_t)n);
        strcpy(strs[1], &str[n]);
        return 0;
    }
}