#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
//#include <dlfcn.h>
#include "library/string_custom.h"

void* handle;

int main(int argc, char** argv)
{
    /// Подключение динамической библиотеки
    /*int (*concat)(char**, char**, char**);
    int (*split)(char**, char*, int, int);
    handle = dlopen("library/libstring_custom.so", RTLD_LAZY);
    if(!handle) {
        printf("Dynamic library doesn't exist!\n");
        return 1;
    }
    concat = dlsym(handle, "concat");
    split = dlsym(handle, "split");
    */
    char *oper = argv[1]; // Считывание из консоли команды
    if(strcmp(oper, "concat") == 0) { // Если выбрана склейка строки
        printf("Concatenate string.\n");
        char* str1 = argv[2];
        char *str2 = argv[3];
        char* str_c;
        concat(&str1, &str2, &str_c);
        printf("%s\n", str_c);
        free(str_c);
    }
    if(strcmp(oper, "split") == 0) { // Если выбран разрыв строки
        printf("Split string.\n");
        char* str = argv[2];
        int split_pos = atoi(argv[3]);
        char** strs = malloc(2*sizeof(char*));
        if(split(strs, str, split_pos, 30) == 1) {
            printf("The string so short.\n");
            //dlclose(handle);
            return 1;
        } else {
            printf("1)%s\n", strs[0]);
            printf("2)%s\n", strs[1]);
        }
        free(strs[0]);
        free(strs[1]);
        free(strs);
    }
    //dlclose(handle);
    return 0;
}